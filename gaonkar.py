#!/usr/bin/python3

import requests
from multiprocessing.dummy import Pool as ThreadPool
from os import listdir, getcwd, makedirs
from os.path import isfile, join, exists
import img2pdf

print("[INFO] Pinging server")
response = requests.get("https://www.scribd.com/document/350971544/Microprocessor-architecture-programming-and-applications-with-the-8085-by-Ramesh-S-Gaonkar-pdf")

print("[INFO] Decoding JSON URLs")
jsonurl = []
lines = str(response.content).split("\\n")
for line in lines:
    if "pageParams.contentUrl" in line:
        line = line.strip("pageParams.contentUrl = \"")
        line = line.strip("\";")
        jsonurl.append(line)
        #f2.write(str(res.content))

def get_img_url(jsonurl):
    print("[INFO] GETing : " + jsonurl)
    response = requests.get(jsonurl)
    print("[INFO] DONE   : " + jsonurl + " : " + str(response))
    return response.content

print("[INFO] Getting image URLs")
pool = ThreadPool(20)
imgurls = pool.map(get_img_url, jsonurl)
pool.close()
pool.join()

currentdir = getcwd()
picdir = join(currentdir, 'pics')
if not exists(picdir):
    print("[INFO] Creating new directory")
    makedirs(picdir)

def payload_func(url):
    name = url.split("/")[-1]
    pid = "%03d" % int(name.split("-")[0])
    print("GETing : " + pid)
    response = requests.get(url)
    print("DONE   : " + pid + " : " + str(response))
    img = open(join(picdir, pid + ".jpg"), "wb")
    img.write(response.content)
    img.close()

print("[INFO] Decoding image URLs")
urls = []
for content in imgurls:
    parts = str(content).split("=\\\\\"")
    for part in parts:
        if "http://" in part:
            part = part.split("\\\\\"/>")
            urls.append(part[0])

print("[INFO] Downloading images")
pool = ThreadPool(20)
pool.map(payload_func, urls)
pool.close()
pool.join()

print("[INFO] Creating Gaonkar.pdf")
onlyfiles = [f for f in listdir(picdir) if isfile(join(picdir, f))]
onlyfile = []
for f in sorted(onlyfiles):
    onlyfile.append(join(picdir, f).encode())
with open("Gaonkar.pdf","wb") as f:
    f.write(img2pdf.convert(onlyfile))

