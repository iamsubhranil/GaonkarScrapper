## python3

1. Install img2pdf using pip

```
    pip3 install --user img2pdf
```

2. Run this script

```
    python3 gaonkar.py
```

3. Collect the pdf from this folder
